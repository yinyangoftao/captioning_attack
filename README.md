**Image Captioning Attack Brief Report**
===

Introduction
============

Adversarial examples are images delicately constructed images used to
fool computer vision systems. They could be gotten by adding
perturbations to original queried images, and could hardly be
distinguished from the original/benign ones. Since it was discovered in
2014, it has been a hot topic because the fatal threat it brings to the
widely used deep networks. The procedure of constructing adversarial
examples for given outputs are called adversarial attacks, and the
attempt to get results consistent to benign counterparts from
adversarial examples are referred to as adversarial defenses.
Adversarial examples for classifiers has been widely studied, among
which the fastest (but not strong) one is FGSM, and C&W is a popular
strong but slow attack method. Since these 2 methods are well know in
this domain, I skip them here and recommend anyone interested check the
reference chapter.
For other systems such as detection systems as well as captioning
systems, there’re few works. My work focuses on image captioning
systems. An influential image captioning work is show-and-tell, based on
which there is a code-available work called show-and-fool aiming at
fooling the show-and-tell system.
Based on show-and-fool, I did some experiment aiming at figuring out the
similarities and differences between different tasks (in my case
captioning and classification), via inspecting attacks and defenses on
them.

Details of the approach {#details-of-the-approach .unnumbered}
=======================

I explored 2 sub questions:

1.  Transferability of attack between tasks

2.  Defense of captioning networks  

The procedure for the attack branch is:

1.  Test whether classification adversarial examples are transferable to
    image captioning task.

    1.  Generate adversarial examples for classification task, using a
        certain type of untargeted attacking method. (I tried FGSM and
        C&W attack).

            classifier = ClfModel()
            attack_params = get_attack_params()
            orig_imgs = load_imgs(path_to_benign_images)
            Adversary = ClfAttacker(classifier,attack_params)
            adv_imgs = Adversary.attack(orig_imgs)

    2.  For each adversarial image, input it to the image captioning
        network which uses Show-and-Fool implementation and generate
        captions for these adversarial images and their corresponding
        original/benign images.

          CaptionGenerator = ShowAndTellModel()
          orig_captions = CaptionGenerator.generate_captions(orig_imgs)
          adv_caption = CaptionGenerator.generate_captions(adv_imgs)

    3.  Compare the robustness based on BLEU score and exact word
        matching.

            trans_clf2cap_result = compare_captions(orig_captions,adv_caption)

2.  Test whether classification adversarial examples are transferable to
    image captioning task.

    1.  Generate adversarial examples for captioning task, using a
        certain type of attacking method. (I tried targeted C&W attack
        only).

            CaptionGenerator = ShowAndTellModel()
            attack_params = get_attack_params()
            orig_imgs = load_imgs(path_to_benign_images)
            Adversary = CaptionAttacker(CaptionGenerator,attack_params)
            adv_imgs = Adversary.attack(orig_imgs)

    2.  For each adversarial image, input it to the image captioning
        network which uses Show-and-Fool implementation and generate
        captions for these adversarial images and their corresponding
        original/benign images.

            classifier = ClfModel()
            orig_labels = classifier.generate_captions(orig_imgs)
            adv_labels = classifier.generate_captions(adv_imgs)

    3.  Compare the robustness based on BLEU score and exact word
        matching.

            trans_cap2clf_result = compare_labels(orig_labels, adv_labels)

The procedure for the defense branch is:

1.  Test the success rate of getting adversarial images. The
    Show-and-Fool paper provides code for this step, which measures the
    distortion, word-exact attack rate etc. I simply added another
    cirterion – BLEU score.

        CaptionGenerator = ShowAndTellModel()
        attack_params = get_attack_params()
        orig_imgs = load_imgs(path_to_benign_images)
        Adversary = CaptionAttacker(CaptionGenerator,attack_params)
        adv_imgs = Adversary.attack(orig_imgs)
        orig_captions = CaptionGenerator.generate_captions(orig_imgs)
        adv_caption = CaptionGenerator.generate_captions(adv_imgs)
        adv_result = eval_caption(orig_captions,adv_caption)

2.  Test whether the adversarial images could be defended by storing the
    images to unsigned 8-bit interger (png format) and read in again
    (uint defense).

        uint_imgs = store2uint_and_read(adv_imgs)
        uint_captions = CaptionGenerator.generate_captions(uint_imgs)
        uint_result = eval_caption(orig_captions,uint_captions)

3.  Test whether the adversarial images could be defended by denoising
    with randomization (randomization defense). Meanwhile also need to
    test the corresponding benign/original images.

        dfs_imgs = randomization_defense(adv_imgs)
        dfs_captions = CaptionGenerator.generate_captions(dfs_imgs)
        dfs_orig_imgs = randomization_defense(orig_imgs)
        dfs_orig_captions = CaptionGenerator.generate_captions(dfs_orig_imgs)
        dfs_orig_result = eval_defense(orig_captions,dfs_captions,dfs_orig_captions)

    Here the
    [randomization defense](https://github.com/cihangxie/NIPS2017_adv_challenge_defense)
    is based on the 2nd place solution of [NIPS 2017: Defense Against
    Adversarial
    Attack](https://www.kaggle.com/c/nips-2017-defense-against-adversarial-attack).
    The winner of this Challenge simply studied FGSM and PGD(projected
    gradient descent, a iterative version of FGSM) and cannot defend C&W
    attack. Note that here the randomization based defense is model
    ignorant – the procedure is random-sizing the given image, adding a
    random-sized padding and reshaping it back to the original size.

4.  Test whether the adversarial images could be defended by first
    detecting the adversarial examples and then apply randomization
    defense to those images believed to be adversarial ones.

        dfs_imgs = detect_and_defense(adv_imgs)
        dfs_captions = CaptionGenerator.generate_captions(dfs_imgs)
        dfs_orig_imgs = detect_and_defense(orig_imgs)
        dfs_orig_captions = CaptionGenerator.generate_captions(dfs_orig_imgs)
        dfs_orig_result = eval_defense(orig_captions,dfs_captions,dfs_orig_captions)

    The detect_and_defense function is based on the [RBF-SVM
    based detector](https://arxiv.org/abs/1704.00103) proposed by Jiajun
    et.al. It executes as follows:

    1.  Build a feature extractor, which gives a feature code of an
        image by quantizing the neurons of a certain middle layer of a
        neural network.

    2.  For any given input image (denote as img), extract its feature.

    3.  Use a trained RBF-SVM to determine whether img is adversarial
        example or it’s a benign one.

    4.  If it’s judged as a benign one, return img it self, otherwise
        return randomization_defense(img).

As an illustration, here is a set of demo images:

  ---------------------------------------------------------------------------- --------
   ![original image ](defense/original_COCO_val2014_000000000073.png "original image")
   ![adversarial image for classification task](defense/clf_adv.png "adversarial image for classification task")

original image v.s. adversarial image for classification task

  ---------------------------------------------------------------------------- -------------------------------------------------

 ![np_adv_img](defense/np_adv_img.png) ![dfs_adv](defense/dfs_adv.png)
 ![dfs_orig](defense/dfs_orig.png)
 adversarial image for captioning task v.s. randomization defended
 adversarial image for captioning task v.s. randomization defended original
 image


The pseudo code basically described what did I test, but the actual code
logic might be different. More specifically:

-   run_eval_BATCH.py evaluates transferability from
    classification to captioning networks, as well as defense of
    captioning networks.

-   run_clf_eval.py evaluates transferability from captioning
    networks to classifiers.

-   Folder gene_attack_for_incep_v3 is used to
    generate adversarial examples for classifiers.

-   feature_extractor.py is used to extract features from images.

I used the released code of Show-and-Fool paper to get adversarial
examples for the captioning system.

Data and Experiment Setting
===========================

I used MSCOCO 2014 validation set. Since the generation of adversarial
examples through C&W attack is pretty slow, I tested only 64 images when
studying how transferable the attack for classifiers are to captioning
nets. For FGSM, I tested 800 images.
To speed up the attack of captioning system, I set the inner iteration
number to 300 instead of the default value 1000, and this may partially
leads to that few images cannot be attacked successfully.
The randomization defense is slightly modified: in the original work
they use random sized (of course within a range, 310 to 331) defended
images, but due to limitation of input images, I reshape the defended
images back to (299,299).
For captioning nets, the only part requiring training is adversarial
detector. I generated adversarial examples for 1200 images to train the
RBF-SVM. The experiment of transferability and defense of captioning
networks are based on the performance on another 800 images.
The models used for classifier/image encoder of captioning
network/feature extractor of adversarial examples detector are all
inception v3. For feature extractor of adversarial examples detector,
the average-pooled neurons after Mixed_7c layer is used to get the
code which is quantized later into 8 possible bins.

Results
=======

The result of transferability is as follows:

For the experiment of measuring transferability of attacks from image
captioning task to classification task, I generated 1 adversarial
example for each of the 800 tested images, only 42 adversarial examples
(5.25%) leads to different labels from their corresponding benign
images, and if we store the adversarial images into 8-bit unsigned
integers and read in again, 53 (6.625%) uint8 adversarial images get
different class labels.
In terms of transferability of attacks from classification task to image
captioning task, the attack rate (percentage of adversarial images for
classifier that could also fool the captioning network and lead to a
different caption) is about 38.625%, and if we measure by BLEU score,
using the caption of original images as reference, the result is:

-   adversarial images achieve an average BLEU score of 0.8599040795,

-   8-bit unit defended images achieve 0.8412624928,

-   the BLEU score for randomization defended adversarial images
    averages to 0.8628893077,

-   0.8872871339 for randomization defended original images.

For the defense of adversarial attack on captioning network, the average
result measured with BLEU score is:\

-   0.294594 for adversarial images

-   0.807713 for 8-bit unit defended adversarial images

-   0.8247113 for randomization defended adversarial images

-   0.8829400 for randomization defended original images

To get a sense about whether detecting before randomization-denoise is
reasonable, I compared the averaged BLEU score/exact defense rate of
adversarial and original images, under detecting-before-denoising and
denoising-directly settings. The result is:\

  --------------- ----------------------- ------------------
  | Method | Detect before Denoise | Denoise directly|
|-----------------|:-------------|:---------------:|---------------:|
| BLEU      |  0.8055    | 0.8538   |
| Exact defense |   0.5681 |      0.4893|
  --------------- ----------------------- ------------------

See detailed result here if anyone is interested:

1.  [detect and
    denoise](https://drive.google.com/open?id=1tH0NYJunfccO6r-68C4DsY4bqjKe_lo6)

2.  [without detection, denoise
    directly](https://drive.google.com/open?id=1ZY47z6sg5fCGF8XZQlOoGtL69cTdPhLm)

I also tested the classification precision of the adversarial detector.
Around 70% (among 800 + 800 = 1,600) images could be correctly
classified as benign/adversarial.

Discussion and conclusions
==========================

From the result listed above, we might safely draw the conclusion that
**attack is not or only weakly transferable between classification and
captioning tasks**: on one hand, only a tiny portion of the adversarial
experiments for captioning could also fool classifier I used, while the
success rate of attacking that classifier could achieve 75% with FGSM
and close to 100% with C&W attack. On the other hand, less than 40% of
adversarial examples from classification tasks could fool captioning
caption. This rate seems ok, but if we look at BLEU score, we can see
that the BLEU scores are above 80%, which indicates that although the
generated captions changed, the meaning of those captions basically
maintains. This conclusion is reasonable and natural: adversarial
examples mentioned in this report are based on the loss functions of
each task, and adversarial examples work by moving the neurons toward
directions that to the largest extent balance raising influences on the
loss function while keeping the perturbations small. Since the loss
functions are different for different tasks, it’s natural that few
perturbations plotted for one task have significant enough influence for
another task.
In terms of defense of image captioning systems, my experiment indicates
that although the success rate of attack is pretty high and could
significantly change the meaning of generated captions, they could be
easily defensed by using methods even as naive as 8-bit integer defense.
It’s reasonable that this defense works, since it provides a kind of
gradient mask.
The experiments also indicates that randomization based defense is
slightly better compared to 8-int unsigned integer defense. While adding
a detection step before randomization denoising cannot improve the
result in terms of BLEU score, it’s obviously helpful for exact defense,
since the benign images wouldn’t get modified if correctly classified
into benign class. This conclusion is generally useful since it also
indicates such step might perhaps be also useful for other defense
methods.
Of course there are some drawbacks of my experiments and some possible
directions to work on in the future:

1.  A more reasonable plan is to use adversarial training or model
    distillation, since it can help verify whether defense is
    transferable between tasks. The methods used in paper are
    model-ignorant, so they’re actually just data preprocessing
    techniques, which don’t make the model itself more robust. However
    due to the limit of time, I only tried adding detection step before
    random-denoising.

2.  The volumes of both training data and testing data are too small.
    This is also caused by lacking of computational power and time. I
    may implement FGSM version of caption attacks.

3.  More extensive experiments could be conducted such as modify the
    detector. The detector it self is not necessarily ought to be the
    same with the classifier or image encoder of captioning network –
    since it’s used to capture unusual images by inspecting the
    amplified abnormal features at a certain layer of a certain network,
    what matters is whether the abnormal features could be captured
    instead of what network we use to get those features.

References
==========

Dataset I used:

1.  [MSCOCO2014 validation set](http://cocodataset.org/#download)

External code I used or modified from:

1.  [Show-and-Fool](https://github.com/huanzhang12/ImageCaptioningAttack)

2.  [Mitigating Adversarial Effects Through
    Randomization](https://github.com/cihangxie/NIPS2017_adv_challenge_defense)

Other papers relative to this work:

1.  [ Explaining and Harnessing Adversarial
    Examples](https://arxiv.org/abs/1412.6572) describes FGSM method

2.  [Towards Evaluating the Robustness of Neural
    Networks](https://arxiv.org/abs/1608.04644) describes C&W method

3.  [SafetyNet: Detecting and Rejecting Adversarial Examples
    Robustly](https://arxiv.org/abs/1704.00103) describes building an
    RBF-SVM to detect adversarial examples
