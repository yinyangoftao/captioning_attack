# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Generate captions for images using default beam search parameters."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import os
import re
import csv
# from tqdm import tqdm
import random
import numpy as np
import tensorflow as tf

import configuration
import inference_wrapper
# import run_detec_code_rbfsvm
import torch
from torch.autograd import Variable as Variable
from feature_extractor import InceptionFeatureExtractor
from inference_utils import caption_generator
from inference_utils import vocabulary
import matplotlib.pyplot as plt
import torchvision.models as models
from nltk.translate.bleu_score import sentence_bleu
import pickle
from sklearn import svm
import copy

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string("checkpoint_path", "pretrained/model2.ckpt-2000000",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")

tf.flags.DEFINE_string("vocab_file", "pretrained/word_counts.txt", 
                      "Text file containing the vocabulary.")

tf.flags.DEFINE_string("orig_dir", "",
                       "Input original image directory of original images.")

tf.flags.DEFINE_string("adv_dir", "",
                       "Input attacked image directory of original images.")

tf.flags.DEFINE_string("dfs_dir", "./pretrained/detector.pkl",
                       "Input defended attacked image directory of original images.")

tf.flags.DEFINE_string("defense_plan", "randomization",
                       "What kind of defense to use.")

tf.flags.DEFINE_string("detector_path", "./pretrained/detector.pkl",
                       "Path of adversarial detector.")

tf.flags.DEFINE_string("result_directory", "../result_dir/",
                        "the directory to save results")

tf.flags.DEFINE_bool(
    'defend_orig', True, 'Whether defend original image.')

tf.flags.DEFINE_bool(
    'torch2_01', False, 'Convert from torch value domain to [0,1] domain.')

tf.flags.DEFINE_integer("beam_size", 3, "beam search size")

tf.flags.DEFINE_integer(
    'image_width', 299, 'Width of each input images.')

tf.flags.DEFINE_integer(
    'image_height', 299, 'Height of each input images.')

tf.flags.DEFINE_integer(
    'batch_size', 1, 'How many images process at one time.')

tf.flags.DEFINE_integer(
    'image_resize', 331, 'Resize of image size.')

tf.logging.set_verbosity(tf.logging.INFO)

def padding_layer_iyswim(inputs, shape, name=None):
    # resize the image and pad it, the returned image would be resized to a square again
    # inputs is the size of the resized image
    # shape is [vertical start padding len, horizontal start padding len, output short len]

    h_start = shape[0]
    w_start = shape[1]
    output_short = shape[2]
    input_shape = tf.shape(inputs)
    input_short = tf.reduce_min(input_shape[1:3])
    input_long = tf.reduce_max(input_shape[1:3])
    output_long = tf.to_int32(tf.ceil(
        1. * tf.to_float(output_short) * tf.to_float(input_long) / tf.to_float(input_short)))
    output_height = tf.to_int32(input_shape[1] >= input_shape[2]) * output_long +\
        tf.to_int32(input_shape[1] < input_shape[2]) * output_short
    output_width = tf.to_int32(input_shape[1] >= input_shape[2]) * output_short +\
        tf.to_int32(input_shape[1] < input_shape[2]) * output_long
    return tf.pad(inputs, tf.to_int32(tf.stack([[0, 0], [h_start, output_height - h_start - input_shape[1]], [w_start, output_width - w_start - input_shape[2]], [0, 0]])), name=name)

def main(_):
  # Build the inference graph.
  # import ipdb; ipdb.set_trace()
  record_path = FLAGS.result_directory
  beam_size = FLAGS.beam_size
  mean_torch_np = np.array([0.485, 0.456, 0.406]).reshape(1,1,3)
  std_torch_np = np.array([0.229, 0.224, 0.225]).reshape(1,1,3)
  mean_tf_np = np.array([0.5, 0.5, 0.5]).reshape(1,1,3)
  std_tf_np = np.array([0.5, 0.5, 0.5]).reshape(1,1,3)

  if FLAGS.detector_path != "":
    bins = np.arange(-1,1,0.4)
    net = models.inception_v3(pretrained=True).eval().cuda()
    code_extractor = InceptionFeatureExtractor(net)
    detector = pickle.load(open(FLAGS.detector_path,"rb"))
  g = tf.Graph()
  with g.as_default():
    model = inference_wrapper.InferenceWrapper()
    restore_fn = model.build_graph_from_config(configuration.ModelConfig(),
                                               FLAGS.checkpoint_path)
    # preprocessing compute graph
    image_placeholder = tf.placeholder(dtype=tf.string, shape=[])
    preprocessor = model.model.process_image(image_placeholder)

    #padding for defense
    batch_shape = [FLAGS.batch_size, FLAGS.image_height, FLAGS.image_width, 3]
    x_input = tf.placeholder(tf.float32, shape=batch_shape)
    img_resize_tensor = tf.placeholder(tf.int32, [2])
    x_input_resize = tf.image.resize_images(x_input, img_resize_tensor, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    shape_tensor = tf.placeholder(tf.int32, [3])
    padded_input = padding_layer_iyswim(x_input_resize, shape_tensor)
    # 330 is the last value to keep 8*8 output, 362 is the last value to keep 9*9 output, stride = 32
    padded_input.set_shape(
        (FLAGS.batch_size, FLAGS.image_resize, FLAGS.image_resize, 3))
    resize_back_tensor = tf.placeholder(tf.int32, [2])
    padded_resize_back = tf.image.resize_images(padded_input, resize_back_tensor, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

  g.finalize()

  # Create the vocabulary.
  vocab = vocabulary.Vocabulary(FLAGS.vocab_file)
  # 8-bit iamges
  # orig_filenames = [file for file in os.listdir(FLAGS.orig_dir+'/origs/')]
  adv_filenames = [file for file in os.listdir(FLAGS.adv_dir+'/advs/')]
  # real valued images
  orig_npy_filenames = [file for file in os.listdir(FLAGS.orig_dir+'/origs_npy/')]
  adv_npy_filenames = [file for file in os.listdir(FLAGS.adv_dir+'/advs_npy/')]
  # orig_filenames.sort()
  adv_filenames.sort()
  orig_npy_filenames.sort()
  adv_npy_filenames.sort()
  if FLAGS.defense_plan == 'from_file':
    assert (FLAGS.dfs_dir != '') and "specify the defense folder!"
    dfs_filenames = [file for file in os.listdir(FLAGS.dfs_dir+'/dfs/')]
    dfs_filenames.sort()
  tf.logging.info("Running caption generation on %d files matching %s",
                  len(orig_npy_filenames), FLAGS.orig_dir)

  header = ("target filename","L2 distortion of precise image","L2 distortion of 8-bit image","integer defendable?","denoise defendable?","defended original fine?","BLEU adv","BLEU adv uint 8","BLEU defended adv","BLEU defended orig","adv_as_benign","orig_as_benign")#,"attack successful?", "8int attack successful?")
  header += tuple([val for pair in zip(["caption before attack "+str(i+1) for i in range(beam_size)], ["prob of caption before attack "+str(i+1) for i in range(beam_size)]) for val in pair])
  header += tuple([val for pair in zip(["caption after attack "+str(i+1) for i in range(beam_size)], ["prob of caption after attack "+str(i+1) for i in range(beam_size)]) for val in pair])
  header += tuple([val for pair in zip(["caption after int8 attack "+str(i+1) for i in range(beam_size)], ["prob of caption after int8 attack "+str(i+1) for i in range(beam_size)]) for val in pair])
  header += tuple([val for pair in zip(["caption of attacked npy image after defense"+str(i+1) for i in range(beam_size)], ["prob of caption after defend attacked "+str(i+1) for i in range(beam_size)]) for val in pair])
  if FLAGS.defend_orig:
    header += tuple([val for pair in zip(["caption of original npy image after defense"+str(i+1) for i in range(beam_size)], ["prob of caption after defend original "+str(i+1) for i in range(beam_size)]) for val in pair])
  record = open(record_path, "a+") #os.path.join(record_path, "record_defense.csv"),"a+")
  writer = csv.writer(record)
  writer.writerow(header)
  record.close()
  with tf.Session(graph=g) as sess:
    # Load the model from checkpoint.
    restore_fn(sess)
    # Prepare the caption generator. Here we are implicitly using the default
    # beam search parameters. See caption_generator.py for a description of the
    # available beam search parameters.
    generator = caption_generator.CaptionGenerator(model, vocab)
    for i in range(len(adv_npy_filenames)):
      print(i)
      np_orig_img = np.squeeze(np.load(FLAGS.orig_dir+'origs_npy/'+orig_npy_filenames[i]))
      np_adv_img = np.squeeze(np.load(FLAGS.adv_dir+'advs_npy/'+adv_npy_filenames[i]))
      if FLAGS.torch2_01:
        np_adv_img = (np.clip(np_adv_img * std_torch_np + mean_torch_np, 0,1) - 0.5)/0.5
      if FLAGS.defense_plan == 'randomization':
        resize_shape_ = np.random.randint(310, 331)
        defend_this = True
        if FLAGS.detector_path != "":
            torch_adv_img = ((np_adv_img * std_tf_np + mean_tf_np) - mean_torch_np)/std_torch_np
            torch_adv_img = Variable(torch.Tensor(torch_adv_img).cuda(), volatile = True).permute([2,0,1]).unsqueeze(0)
            code = code_extractor(torch_adv_img).view(1,-1).cpu().data.numpy()
            quantized_code = np.digitize(code, bins)
            adv_is_benign = detector.predict(quantized_code)[0]
            if adv_is_benign > 0.5:
              defend_this = False
        if defend_this:
          import ipdb; ipdb.set_trace()
          dfs_adv = sess.run(padded_resize_back,
            feed_dict={x_input: np.expand_dims(np_adv_img,0), img_resize_tensor: [resize_shape_]*2, resize_back_tensor: [FLAGS.image_width, FLAGS.image_height],
            shape_tensor: np.array([random.randint(0, FLAGS.image_resize - resize_shape_), random.randint(0, FLAGS.image_resize - resize_shape_), FLAGS.image_resize])})
          dfs_adv = dfs_adv.squeeze()
        else:
          dfs_adv = copy.deepcopy(np_adv_img)
        if FLAGS.defend_orig:
          defend_this = True
          if FLAGS.detector_path != "":
            torch_orig_img = ((np_orig_img * std_tf_np + mean_tf_np) - mean_torch_np)/std_torch_np
            torch_orig_img = Variable(torch.Tensor(torch_orig_img).cuda(), volatile = True).permute([2,0,1]).unsqueeze(0)
            code = code_extractor(torch_orig_img).view(1,-1).cpu().data.numpy()
            quantized_code = np.digitize(code, bins)
            orig_is_benign = detector.predict(quantized_code)[0]
            if orig_is_benign > 0.5:
              defend_this = False
          if defend_this:
            dfs_orig = sess.run(padded_resize_back,
            feed_dict={x_input: np.expand_dims(np_orig_img,0), img_resize_tensor: [resize_shape_]*2,resize_back_tensor: [FLAGS.image_width, FLAGS.image_height],
            shape_tensor: np.array([random.randint(0, FLAGS.image_resize - resize_shape_), random.randint(0, FLAGS.image_resize - resize_shape_), FLAGS.image_resize])})
            dfs_orig = dfs_orig.squeeze()
          else:
            dfs_orig = copy.deepcopy(np_orig_img)
      elif FLAGS.defense_plan == 'from_file':
        dfs_adv = np.squeeze(np.load(FLAGS.dfs_dir+'dfs/' + dfs_filenames[i]))
        if FLAGS.defend_orig:
          dfs_orig = np.squeeze(np.load(FLAGS.dfs_dir+'dfs_orig/' + dfs_filenames[i]))
      # with tf.gfile.GFile(FLAGS.orig_dir+'/origs/'+orig_filenames[i], "rb") as f:
      #   orig_img = f.read()
      #   orig_img = sess.run(preprocessor, {image_placeholder: orig_img})
      with tf.gfile.GFile(FLAGS.adv_dir+'advs/'+adv_filenames[i], "rb") as f:
        unit8_adv = f.read()
        unit8_adv = sess.run(preprocessor, {image_placeholder: unit8_adv})
      # print('raw image shape is', np_orig_img.shape)
      
      orig_sentences = []
      adv_int8_sentences = []
      adv_sentences = []
      dfs_sentences = []
      dfs_orig_sentences = []

      orig_probs = []
      adv_int8_probs = []
      adv_probs = []
      dfs_probs = []
      dfs_orig_probs = []

      orig_captions = generator.beam_search(sess, np_orig_img)
      for j, caption in enumerate(orig_captions):
        # Ignore begin and end words.
        sentence = [vocab.id_to_word(w) for w in caption.sentence[1:-1]]
        sentence = " ".join(sentence)
        orig_sentences += [sentence]
        orig_probs += [math.exp(caption.logprob)]

      np_adv_captions = generator.beam_search(sess, np_adv_img)
      for j, caption in enumerate(np_adv_captions):
        # Ignore begin and end words.
        sentence = [vocab.id_to_word(w) for w in caption.sentence[1:-1]]
        sentence = " ".join(sentence)
        adv_sentences += [sentence]
        adv_probs += [math.exp(caption.logprob)]

      adv_captions = generator.beam_search(sess, unit8_adv)
      for j, caption in enumerate(adv_captions):
        # Ignore begin and end words.
        sentence = [vocab.id_to_word(w) for w in caption.sentence[1:-1]]
        sentence = " ".join(sentence)
        adv_int8_sentences += [sentence]
        adv_int8_probs += [math.exp(caption.logprob)]

      dfs_captions = generator.beam_search(sess, dfs_adv)
      for j, caption in enumerate(dfs_captions):
        # Ignore begin and end words.
        sentence = [vocab.id_to_word(w) for w in caption.sentence[1:-1]]
        sentence = " ".join(sentence)
        dfs_sentences += [sentence]
        dfs_probs += [math.exp(caption.logprob)]

      if FLAGS.defend_orig:
        dfs_orig_captions = generator.beam_search(sess, dfs_orig)
        for j, caption in enumerate(dfs_orig_captions):
          # Ignore begin and end words.
          sentence = [vocab.id_to_word(w) for w in caption.sentence[1:-1]]
          sentence = " ".join(sentence)
          dfs_orig_sentences += [sentence]
          dfs_orig_probs += [math.exp(caption.logprob)]
        orig_denoised_fine = dfs_orig_sentences[0] == orig_sentences[0]
        bleu_dfs_orig = sentence_bleu(dfs_orig_sentences,orig_sentences[0])
      # use bleu score here?
      # np_success = adv_sentences[0] == orig_sentences[0]
      # could we solve the adversarial issue simply by pushing it to the closest integer?
      # measure this via BLEU later
      int_defendable = adv_int8_sentences[0] == orig_sentences[0]
      denoise_success = dfs_sentences[0] == orig_sentences[0]
      
      distortion = np.sum((np_adv_img - np_orig_img)**2)**.5
      distortion_int8 = np.sum((unit8_adv - np_orig_img)**2)**.5

      bleu_adv = sentence_bleu(adv_sentences,orig_sentences[0])
      bleu_uint_adv = sentence_bleu(adv_int8_sentences,orig_sentences[0])
      bleu_dfs_adv = sentence_bleu(dfs_sentences,orig_sentences[0])
      
      # write results
      record = open(record_path, "a+")#open(record_path + "record_defense.csv","a+")
      writer = csv.writer(record)
      row = (orig_npy_filenames[i].split(".")[0],distortion,distortion_int8, str(int_defendable), str(denoise_success), str(orig_denoised_fine))#,str(np_success),str(int_success))
      row += (bleu_adv, bleu_uint_adv, bleu_dfs_adv, bleu_dfs_orig, adv_is_benign, orig_is_benign)
      row += tuple([val for pair in zip(orig_sentences, orig_probs) for val in pair])
      row += tuple([val for pair in zip(adv_sentences, adv_probs) for val in pair])
      row += tuple([val for pair in zip(adv_int8_sentences, adv_int8_probs) for val in pair])
      row += tuple([val for pair in zip(dfs_sentences, dfs_probs) for val in pair])
      if FLAGS.defend_orig:
        row += tuple([val for pair in zip(dfs_orig_sentences, dfs_orig_probs) for val in pair])
      writer.writerow(row)
      record.close()

if __name__ == "__main__":
  tf.app.run()
