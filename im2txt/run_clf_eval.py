# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Generate captions for images using default beam search parameters."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import os
import re
import csv
# from tqdm import tqdm
import random
import numpy as np
import tensorflow as tf

import configuration
import inference_wrapper
from inference_utils import caption_generator
from inference_utils import vocabulary
import torch
from torch.autograd import Variable as Variable
import matplotlib.pyplot as plt
import torchvision.models as models

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string("checkpoint_path", "pretrained/model2.ckpt-2000000",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")

tf.flags.DEFINE_string("vocab_file", "pretrained/word_counts.txt", 
                      "Text file containing the vocabulary.")

tf.flags.DEFINE_string("orig_dir", "",
                       "Input original image directory of original images.")

tf.flags.DEFINE_string("adv_dir", "",
                       "Input attacked image directory of original images.")

tf.flags.DEFINE_string("dfs_dir", "",
                       "Input defended attacked image directory of original images.")

tf.flags.DEFINE_string("defense_plan", "",
                       "What kind of defense to use.")

tf.flags.DEFINE_string("result_directory", "../result_dir/",
                        "the directory to save results")

tf.flags.DEFINE_bool(
    'defend_orig', True, 'Whether defend original image.')

tf.flags.DEFINE_bool(
    'tf2_01', True, 'Convert from torch value domain to [0,1] domain.')

tf.flags.DEFINE_integer("beam_size", 3, "beam search size")

tf.flags.DEFINE_integer(
    'image_width', 299, 'Width of each input images.')

tf.flags.DEFINE_integer(
    'image_height', 299, 'Height of each input images.')

tf.flags.DEFINE_integer(
    'batch_size', 1, 'How many images process at one time.')

tf.flags.DEFINE_integer(
    'image_resize', 331, 'Resize of image size.')

tf.logging.set_verbosity(tf.logging.INFO)

def padding_layer_iyswim(inputs, shape, name=None):
    # resize the image and pad it, the returned image would be resized to a square again
    # inputs is the size of the resized image
    # shape is [vertical start padding len, horizontal start padding len, output short len]

    h_start = shape[0]
    w_start = shape[1]
    output_short = shape[2]
    input_shape = tf.shape(inputs)
    input_short = tf.reduce_min(input_shape[1:3])
    input_long = tf.reduce_max(input_shape[1:3])
    output_long = tf.to_int32(tf.ceil(
        1. * tf.to_float(output_short) * tf.to_float(input_long) / tf.to_float(input_short)))
    output_height = tf.to_int32(input_shape[1] >= input_shape[2]) * output_long +\
        tf.to_int32(input_shape[1] < input_shape[2]) * output_short
    output_width = tf.to_int32(input_shape[1] >= input_shape[2]) * output_short +\
        tf.to_int32(input_shape[1] < input_shape[2]) * output_long
    return tf.pad(inputs, tf.to_int32(tf.stack([[0, 0], [h_start, output_height - h_start - input_shape[1]], [w_start, output_width - w_start - input_shape[2]], [0, 0]])), name=name)

def main(_):
  # Build the inference graph.
  # import ipdb; ipdb.set_trace()
  record_path = FLAGS.result_directory
  beam_size = FLAGS.beam_size
  mean_torch_np = np.array([0.485, 0.456, 0.406]).reshape(1,1,3)
  std_torch_np = np.array([0.229, 0.224, 0.225]).reshape(1,1,3)
  mean_tf_np = np.array([0.5, 0.5, 0.5]).reshape(1,1,3)
  std_tf_np = np.array([0.5, 0.5, 0.5]).reshape(1,1,3)
  net = models.inception_v3(pretrained=True).eval().cuda()
  g = tf.Graph()
  with g.as_default():
    model = inference_wrapper.InferenceWrapper()
    restore_fn = model.build_graph_from_config(configuration.ModelConfig(),
                                               FLAGS.checkpoint_path)
    # preprocessing compute graph
    image_placeholder = tf.placeholder(dtype=tf.string, shape=[])
    preprocessor = model.model.process_image(image_placeholder)

    #padding for defense
    batch_shape = [FLAGS.batch_size, FLAGS.image_height, FLAGS.image_width, 3]
    x_input = tf.placeholder(tf.float32, shape=batch_shape)
    img_resize_tensor = tf.placeholder(tf.int32, [2])
    x_input_resize = tf.image.resize_images(x_input, img_resize_tensor, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    shape_tensor = tf.placeholder(tf.int32, [3])
    padded_input = padding_layer_iyswim(x_input_resize, shape_tensor)
    # 330 is the last value to keep 8*8 output, 362 is the last value to keep 9*9 output, stride = 32
    padded_input.set_shape(
        (FLAGS.batch_size, FLAGS.image_resize, FLAGS.image_resize, 3))
    resize_back_tensor = tf.placeholder(tf.int32, [2])
    padded_resize_back = tf.image.resize_images(padded_input, resize_back_tensor, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

  g.finalize()

  # Create the vocabulary.
  vocab = vocabulary.Vocabulary(FLAGS.vocab_file)
  # 8-bit iamges
  # orig_filenames = [file for file in os.listdir(FLAGS.orig_dir+'/origs/')]
  adv_filenames = [file for file in os.listdir(FLAGS.adv_dir+'/advs/')]
  # real valued images
  orig_npy_filenames = [file for file in os.listdir(FLAGS.orig_dir+'/origs_npy/')]
  adv_npy_filenames = [file for file in os.listdir(FLAGS.adv_dir+'/advs_npy/')]
  # orig_filenames.sort()
  adv_filenames.sort()
  orig_npy_filenames.sort()
  adv_npy_filenames.sort()
  if FLAGS.defense_plan == 'from_file':
    assert (FLAGS.dfs_dir != '') and "specify the defense folder!"
    dfs_filenames = [file for file in os.listdir(FLAGS.dfs_dir+'/dfs/')]
    dfs_filenames.sort()
  tf.logging.info("Running caption generation on %d files matching %s",
                  len(orig_npy_filenames), FLAGS.orig_dir)

  lbl_orig = []
  lbl_adv = []
  lbl_uint = []
  with tf.Session(graph=g) as sess:
    # Load the model from checkpoint.
    restore_fn(sess)
    # Prepare the caption generator. Here we are implicitly using the default
    # beam search parameters. See caption_generator.py for a description of the
    # available beam search parameters.
    for i in range(len(adv_npy_filenames)):
      print(i)
      np_orig_img = np.squeeze(np.load(FLAGS.orig_dir+'origs_npy/'+orig_npy_filenames[i]))
      np_adv_img = np.squeeze(np.load(FLAGS.adv_dir+'advs_npy/'+adv_npy_filenames[i]))
      if FLAGS.tf2_01:
        np_adv_img = ((np_adv_img * std_tf_np + mean_tf_np) - mean_torch_np)/std_torch_np
        np_orig_img = ((np_orig_img * std_tf_np + mean_tf_np) - mean_torch_np)/std_torch_np
      if FLAGS.defense_plan == 'randomization':
        resize_shape_ = np.random.randint(310, 331)
        dfs_adv = sess.run(padded_resize_back,
          feed_dict={x_input: np.expand_dims(np_adv_img,0), img_resize_tensor: [resize_shape_]*2, resize_back_tensor: [FLAGS.image_width, FLAGS.image_height],
          shape_tensor: np.array([random.randint(0, FLAGS.image_resize - resize_shape_), random.randint(0, FLAGS.image_resize - resize_shape_), FLAGS.image_resize])})
        dfs_adv = dfs_adv.squeeze()
        if FLAGS.defend_orig:
          dfs_orig = sess.run(padded_resize_back,
          feed_dict={x_input: np.expand_dims(np_orig_img,0), img_resize_tensor: [resize_shape_]*2,resize_back_tensor: [FLAGS.image_width, FLAGS.image_height],
          shape_tensor: np.array([random.randint(0, FLAGS.image_resize - resize_shape_), random.randint(0, FLAGS.image_resize - resize_shape_), FLAGS.image_resize])})
          dfs_orig = dfs_orig.squeeze()
      elif FLAGS.defense_plan == 'from_file':
        dfs_adv = np.squeeze(np.load(FLAGS.dfs_dir+'dfs/' + dfs_filenames[i]))
        if FLAGS.defend_orig:
          dfs_orig = np.squeeze(np.load(FLAGS.dfs_dir+'dfs_orig/' + dfs_filenames[i]))
      # with tf.gfile.GFile(FLAGS.orig_dir+'/origs/'+orig_filenames[i], "rb") as f:
      #   orig_img = f.read()
      #   orig_img = sess.run(preprocessor, {image_placeholder: orig_img})
      with tf.gfile.GFile(FLAGS.adv_dir+'advs/'+adv_filenames[i], "rb") as f:
        unit8_adv = f.read()
        unit8_adv = sess.run(preprocessor, {image_placeholder: unit8_adv})
      np_orig_img = Variable(torch.Tensor(np_orig_img).cuda()).permute([2,0,1]).unsqueeze(0)
      np_adv_img = Variable(torch.Tensor(np_adv_img).cuda()).permute([2,0,1]).unsqueeze(0)
      unit8_adv = Variable(torch.Tensor(unit8_adv).cuda()).permute([2,0,1]).unsqueeze(0)
      lbl_orig += [net(np_orig_img).max(1)[1]]
      lbl_adv += [net(np_adv_img).max(1)[1]]
      lbl_uint += [net(unit8_adv).max(1)[1]]
    
    lbl_orig = torch.cat(lbl_orig)
    lbl_adv = torch.cat(lbl_adv)
    lbl_uint = torch.cat(lbl_uint)
    print(torch.sum(lbl_orig != lbl_adv))
    print(torch.sum(lbl_orig != lbl_uint))
      


if __name__ == "__main__":
  tf.app.run()
