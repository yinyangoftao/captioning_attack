import os
import sys
import torch
from torch import nn
from torch.autograd import Variable
from utils import *

class FGS_attacker:

    def __init__(self, model, targeted=False, criterion = nn.CrossEntropyLoss(), epsilon0 = 0.1, steps=10, thresh = 1e-3, clip_max = 0.5, clip_min = -0.5):
        """
        model: the model we want to attack
        criterion: used during training phase of the target network
        """
        self.model = model
        self.targeted = targeted
        self.criterion = criterion
        self.epsilon0 = epsilon0
        self.steps = steps
        self.thresh = thresh
        self.clip_max = clip_max
        self.clip_min = clip_min

    def attack(self, img_var, target):
        """
        return the perturbation
        currently process one by one
        img_var: original image, guaranteed to have correct prediction
        target: target label if self.targeted == true, otherwise ground truth label
        """
        # targeted attack
        output = self.model(img_var).squeeze()
        # pred = get_argmax(output,dim = 1)
        if len(output.shape) == 1:
            output = output.unsqueeze(0)
            target = torch.LongTensor([target])
        loss = self.criterion(output, Variable(target.cuda()))
        loss.backward()
        x_grad = torch.sign(img_var.grad.data)
        if self.targeted:
            x_grad = -x_grad
        # search for epsilon
        nimgs = []
        misclf = 0
        for i in range(img_var.shape[0]):
            lo = 0
            hi = self.epsilon0
            while True:
                epsilons = torch.linspace(lo, hi, self.steps)
                epscalars = epsilons.unsqueeze(1).unsqueeze(1).unsqueeze(1).cuda()
                newx = img_var[i].data + epscalars * x_grad[i]
                if len(self.clip_min) == 3:
                    newx_r = newx[:,0,:,:].clamp(self.clip_min[0], self.clip_max[0]).unsqueeze(1)
                    newx_g = newx[:,1,:,:].clamp(self.clip_min[1], self.clip_max[1]).unsqueeze(1)
                    newx_b = newx[:,2,:,:].clamp(self.clip_min[2], self.clip_max[2]).unsqueeze(1)
                    newx = Variable(torch.cat([newx_r, newx_g, newx_b], 1))
                else:
                    newx = Variable(newx.clamp(self.clip_min, self.clip_max))
                outputs = self.model(newx)
                preds = get_argmax(outputs)
                if self.targeted:
                    # TODO targeted not tested
                    lo_idx = torch.nonzero(preds != target[i]).max()
                else:
                    # index of the biggest noise that still correct
                    try:
                        lo_idx = torch.nonzero(preds == target[i]).max()
                    except:
                        # even lowest is not equal to
                        import ipdb;ipdb.set_trace()
                        misclf += 1 
                        nimg = img_var[i].clone()
                        nimgs.append(nimg.unsqueeze(0))
                        break
                # if the biggest noise tested still not successful, abandon this attack and return the original image
                if lo_idx == self.steps - 1:
                    hi = 10 * hi
                    nimg = img_var[i].clone()
                    nimgs.append(nimg.unsqueeze(0))
                    break
                if hi - lo <= self.thresh*self.steps:
                    nimg = newx[lo_idx + 1]
                    nimgs.append(nimg.unsqueeze(0))
                    break
                lo = epsilons[lo_idx]
                hi = epsilons[lo_idx + 1]
        # import ipdb;ipdb.set_trace()
        if misclf > 0:
            print(misclf," misclassified")
        return torch.cat(nimgs)