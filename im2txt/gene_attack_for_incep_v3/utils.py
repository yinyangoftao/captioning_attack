import os

import matplotlib.pyplot as plt
import numpy as np

import torch
from torch import nn
from torch.autograd import Variable
import torchvision

USE_CUDA = True

def plot_tensor(img, fs=(8, 8), title=''):
    # preprocess input
    if type(img) == Variable:
        img = img.data
    img = img.cpu()
    if torch.tensor._TensorBase in type(img).__bases__:
        npimg = img.numpy()
    else:
        npimg = img
    if len(npimg.shape) == 4:
        npimg = npimg[0]
    npimg = npimg.transpose(1, 2, 0)
    plt.figure(figsize=fs)
    if npimg.shape[2] > 1:
        plt.imshow(npimg)
    else:
        npimg = npimg.squeeze()
        plt.imshow(npimg, cmap='gray')
    plt.title(title)
    plt.axis('off')
    plt.show()

def plot_batch(samples):
    sample_grid = torchvision.utils.make_grid(samples)
    plot_tensor(sample_grid)

def one_hotify(x, n_classes=10):
    """
    torch.FloatTensor
    """
    x_long = torch.LongTensor(x)
    x_onehot = torch.sparse.torch.eye(n_classes).index_select(0, x_long)
    return x_onehot

def one_hotify2(x, n_classes = 10, use_cuda = USE_CUDA):
    """
    x: the targets
    """
    target_onehot = torch.zeros(x.size() + (n_classes))
    if use_cuda:
        target_onehot = x.cuda()
    target_onehot.scatter_(1, x.unsqueeze(1), 1.)

def torch_arctanh(x, eps=1e-6):
    x *= (1. - eps)
    return (torch.log((1 + x) / (1 - x))) * 0.5

def arctanh_rescale(y, clip_min = -1., clip_max = 1.):
    """
    such that tanh_rescale(arctanh_rescale(x)) == x
    """
    return torch_arctanh((2*y - clip_max - clip_min)/(clip_max - clip_min))


def l2_norm(diff_imgs):
    N = diff_imgs.shape[0]
    flattened = diff_imgs.view(N, -1)
    return flattened.norm(p = 2, dim = 1)

def l1_norm(diff_imgs):
    N = diff_imgs.shape[0]
    flattened = diff_imgs.view(N, -1)
    return flattened.norm(p = 1, dim = 1)

def rescale(x, dst_min=-1., dst_max=1.):
    scalar = (dst_max - dst_min) / (x.max() - x.min() + 1e-15)
    enlarged = (x - x.min()) * scalar
    # if np.isnan(enlarged):
    #     return dst_max
    return  enlarged + dst_min

def tanh_rescale(x, x_min=-1., x_max=1.):
    return (torch.tanh(x) + 1) * 0.5 * (x_max - x_min) + x_min

def tanh_rescale(x, x_min=-1., x_max=1.):
    return (torch.tanh(x) + 1) * 0.5 * (x_max - x_min) + x_min

def get_argmax(scores,dim=1):
    val, idx = torch.max(scores, dim=dim)
    return idx.data.view(-1).cpu()

def get_accuracy(pred, target):
    correct = torch.sum(pred == target)
    return correct / pred.size(0)

def amplify_diff(diff_img, clip_min, clip_max):
    return torch.clamp(0.5 + 3 * (diff_img), min=-clip_min, max=clip_max)

def avg_norm(diff_imgs):
    return l2_norm(diff_imgs).mean()

def count_params(model):
    return sum(p.numel() for p in model.parameters())

def remove_misclassified(x, y_true, model, idx = False):
    x = Variable(x).cuda()
    y_true = Variable(y_true).cuda()
    outputs = model(x)
    y_preds = get_argmax(outputs)
    correct = y_preds.cuda() == y_true.data
    crc_indices = torch.nonzero(correct).squeeze()
    if(len(crc_indices) <= 0):
        print ("invalid batch, no good prediction!")
        return [],[]
    if idx:
        return x[crc_indices].data.cpu(), y_true[crc_indices].data.cpu(), crc_indices.cpu()
    else:
        return x[crc_indices].data.cpu(), y_true[crc_indices].data.cpu()

def parse_targeted(attack_args):
    targeted = False
    if 'targeted' in attack_args:
        targeted = attack_args['targeted']
    return targeted