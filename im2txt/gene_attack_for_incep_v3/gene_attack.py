"""
used to generate attack so as to verify that the 
model used for captioning is defensive to classification advexamples
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import argparse
import math
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import torch
import torch.autograd as autograd
import torch.utils.data as data
import torchvision
import torchvision.datasets.folder
import torchvision.transforms as transforms
import torchvision.models as models
from utils import *
from attack import *
from dataset import Dataset
from feature_extractor import InceptionFeatureExtractor

parser = argparse.ArgumentParser(description='Defence')
parser.add_argument('--orig_dir', metavar='DIR', default='',
                    help='Input directory with images.')
parser.add_argument('--output_dir', metavar='FILE', default='',
                    help='Output file to save denoised images.')
parser.add_argument('--img-size', type=int, default=299, metavar='N',
                    help='Image patch size (default: 299)')
parser.add_argument('--batch-size', type=int, default=32, metavar='N',
                    help='Batch size (default: 32)')
parser.add_argument('--no-gpu', action='store_true', default=False,
                    help='disables GPU training')
parser.add_argument('--task',type=str,default='denoise', help='task of invokation')
parser.add_argument('--ensemble',type=bool,default=False, help='whether use ensembled model')


class LeNormalize(object):
    """Normalize to -1..1 in Google Inception style
    """

    def __call__(self, tensor):
        for t in tensor:
            t.sub_(0.5).mul_(2.0)
        return tensor

def showtmp(img):
    imgtmp = (img-img.min())/(img.max() - img.min())
    plt.imshow(imgtmp)
    plt.show()
    plt.close()

def show(img, mean_np, std_np, path, name = "output.png", save_np = True):
    #skip if already exist
    if os.path.isfile(os.path.join(path, name)):
        return
    if save_np:
      np.save(path+name, img)
    img = np.clip(img * std_np + mean_np,0,1)
    fig = np.around(img * 255)
    fig = fig.astype(np.uint8).squeeze()
    pic = Image.fromarray(fig)
    # pic.resize((512,512), resample=PIL.Image.BICUBIC)
    pic.save(path+name)

def main():
    args = parser.parse_args()
    # import ipdb;ipdb.set_trace()
    if not os.path.exists(args.orig_dir):
        print("Error: Invalid input folder %s" % args.orig_dir)
        exit(-1)
    if not args.output_dir:
        print("Error: Please specify an output file")
        exit(-1)
        
    tf = transforms.Compose([
           # transforms.Scale([299,299]),
            transforms.ToTensor()
    ])

    mean_torch = autograd.Variable(torch.from_numpy(np.array([0.485, 0.456, 0.406]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    std_torch = autograd.Variable(torch.from_numpy(np.array([0.229, 0.224, 0.225]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    mean_tf = autograd.Variable(torch.from_numpy(np.array([0.5, 0.5, 0.5]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    std_tf = autograd.Variable(torch.from_numpy(np.array([0.5, 0.5, 0.5]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    clipmins = ((-mean_torch)/std_torch).squeeze().cpu().data.numpy()
    clipmins = [np.asscalar(clipmins[0]),np.asscalar(clipmins[1]),np.asscalar(clipmins[2])]
    clipmaxs = ((1-mean_torch)/std_torch).squeeze().cpu().data.numpy()
    clipmaxs = [np.asscalar(clipmaxs[0]),np.asscalar(clipmaxs[1]),np.asscalar(clipmaxs[2])]
    # [-2.11790393,  2.2489083 ][-2.03571429,  2.42857143][-1.80444444,  2.64      ]
    mean_torch_np = np.array([0.485, 0.456, 0.406]).reshape(1,1,3)
    std_torch_np = np.array([0.229, 0.224, 0.225]).reshape(1,1,3)

    dataset = Dataset(args.orig_dir,transform=tf,return_label=False)
    loader = data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,drop_last=False)
    noise_type = os.path.splitext(dataset.filenames()[0])[-1]
    net = models.inception_v3(pretrained=True).eval().cuda()

    attack_args_fgs = {'targeted':False, 'criterion':nn.CrossEntropyLoss(), 'epsilon0': 0.1, 'steps': 10, 'thresh':1e-1, 'clip_max':clipmaxs, 'clip_min':clipmins}
    # attack_args_cw = {'targeted':False, 'confidence': 5, 'initial_const':0.01, 'search_steps': 5, 'max_steps':1000, 'clip_min':-1, 'clip_max':1}
    adversary = Adversary(net, 'FGS', **attack_args_fgs)
    labels_adv = []
    labels_dfs = []
    labels_orig = []
    for batch_idx, (input_, fpath) in enumerate(loader):
        if not args.no_gpu:
            input_ = input_.cuda()
        input_01 = autograd.Variable(input_, volatile=True)
        if noise_type == '.npy':
            input_01 = input_01.permute([0,3,1,2])
            input_01 = input_var * std_tf + mean_tf
        input_torch = (input_01 - mean_torch)/std_torch
        labels3_orig = net(input_torch)
        label_orig = labels3_orig.max(1)[1]
        labels_orig += [label_orig]

        #here due to data without label, we directly use original data's prediction as labels
        tmp_label = label_orig
        fool_img = adversary.attack_batch(input_torch.data, tmp_label.data)
        fool_img = Variable(fool_img.cuda(),volatile=True)
        labels3_adv = net(fool_img)
        label_adv = labels3_adv.max(1)[1]
        labels_adv += [label_adv]
        # save now
        fool_img = fool_img.permute([0,2,3,1]).cpu().data.numpy()
        for i in range(len(fpath)):
            show(fool_img[i], mean_torch_np, std_torch_np, args.output_dir, name = os.path.basename(fpath[i]), save_np = True)

    labels_orig = torch.cat(labels_orig)
    labels_adv = torch.cat(labels_adv)
    attacked = labels_adv != labels_orig
    attack_cnt = len(torch.nonzero(attacked))
    print("{} attacked(clf), out of {}".format(attack_cnt, len(labels_orig)))

if __name__ == '__main__':
    main()
