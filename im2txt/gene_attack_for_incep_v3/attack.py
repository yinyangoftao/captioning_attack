import sys
sys.path.insert(0, 'attacks')
import torch
from torch import nn
from torch.autograd import Variable
from utils import *
from attacks.FGS import FGS_attacker
from attacks.cw_l2 import CarliniWagnerL2_attacker

def set_grad(model, cond):
    for p in model.parameters():
        p.requires_grad = cond

class Adversary():
    
    def __init__(self, model, attack_method, **kwargs):
        self.model = model
        if attack_method == 'FGS':
            self.attacker = FGS_attacker(self.model, **kwargs)
        elif attack_method == 'CW':
            self.attacker = CarliniWagnerL2_attacker(self.model, **kwargs)
        else:
            raise Exception('[!] Unknown attack method specified')

    def attack_batch(self, seed_imgs, target_or_grounds):
        set_grad(self.model, False)
        img_var = Variable(seed_imgs.clone(), requires_grad=True)
        
        # if attack == 'FGS':
            
        #     attacker = FGS_attacker(self.model, **kwargs)
        res = self.attacker.attack(img_var, target_or_grounds)
        # else:
        #     raise Exception('[!] Unknown attack method specified')
        
        set_grad(self.model, True)
        if type(res) == tuple:
            fool_image, iters = res
            return fool_image.data.cpu(), iters
        else:
            fool_image = res
            return fool_image.data.cpu()

def keep_successful_attack(seed_img, fool_img, model, targets = None):
    seed_var = Variable(seed_img).cuda()
    fool_var = Variable(fool_img).cuda()
    scores_seed = model(seed_var)
    pred_seed = scores_seed.data.max(1)[1]
    targeted = not (targets is None)
    # if untargeted, since images are filtered, all are correctly predicted,
    # so target (which we want to avoid for untargeted attack) could be set to pred_seed
    if not targeted:
        targets = pred_seed
    scores_fool = model(fool_var)
    pred_fool = scores_fool.data.max(1)[1]
    if targeted:
        success_idx = torch.nonzero(pred_fool == targets).squeeze()
    else:
        success_idx = torch.nonzero(pred_fool != targets).squeeze()
    if len(success_idx) > 0:
        suc_seed = seed_img[success_idx.cpu()]
        suc_fool = fool_img[success_idx.cpu()]
        diff_imgs = suc_fool - suc_seed
        # result = {
        # 'seed_imgs': suc_seed,
        # 'seed_labels':pred_seed[success_idx].cpu(),
        # 'targets':targets[success_idx].cpu(),
        # 'fool_imgs': suc_fool,
        # 'fool_labels':pred_fool[success_idx].cpu(),
        # 'diff_imgs': diff_imgs}
        return suc_seed,pred_seed[success_idx].cpu(),targets[success_idx].cpu(),suc_fool,pred_fool[success_idx].cpu(),diff_imgs
    else:
        return [],[],[],[],[],[]

def evaluate_example(seed_img, fool_img, model, reconstruction=False):
    seed_var = Variable(seed_img).cuda()
    fool_var = Variable(fool_img).cuda()
    if reconstruction:
        scores_seed, rec_seed = model(seed_var)
        scores_fool, rec_fool = model(fool_var)
    else:
        scores_seed = model(seed_var)
        scores_fool = model(fool_var)
    
    results = {
        'pred_seed': scores_seed.data.cpu().max(1)[1][0],
        'pred_fool': scores_fool.data.cpu().max(1)[1][0]}
    if reconstruction:
        results['mse_seed'] = torch.sum((rec_seed - seed_var)**2).data.cpu()[0]
        results['mse_fool'] = torch.sum((rec_fool - fool_var)**2).data.cpu()[0]
        reconstructions = {
            'rec_seed': rec_seed.data.cpu(),
            'rec_fool': rec_fool.data.cpu()}
        return results, reconstructions
    else:
        return results
