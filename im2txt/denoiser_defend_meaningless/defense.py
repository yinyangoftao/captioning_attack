"""Sample Pytorch defense.

This defense loads inception v3 checkpoint and classifies all images
using loaded checkpoint.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import argparse
import math
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import torch
import torch.autograd as autograd
import torch.utils.data as data
import torchvision
import torchvision.datasets.folder
import torchvision.transforms as transforms

from dataset import Dataset
from res152_wide import get_model as get_model1
from inres import get_model as  get_model2
from v3 import get_model as get_model3
from resnext101 import get_model as get_model4

parser = argparse.ArgumentParser(description='Defence')
parser.add_argument('--noise_dir', metavar='DIR', default='',
                    help='Input directory with images.')
parser.add_argument('--orig_dir', metavar='DIR', default='',
                    help='Input directory with images.')
parser.add_argument('--output_dir', metavar='DIR', default='',
                    help='Output file to save denoised images.')
parser.add_argument('--img-size', type=int, default=299, metavar='N',
                    help='Image patch size (default: 299)')
parser.add_argument('--batch-size', type=int, default=16, metavar='N',
                    help='Batch size (default: 32)')
parser.add_argument('--no-gpu', action='store_true', default=False,
                    help='disables GPU training')
parser.add_argument('--task',type=str,default='denoise', help='task to run. denoise to denoise images and generate denoised captions. label to verify classification result of models')
parser.add_argument('--ensemble',type=bool,default=False, help='whether use ensembled model')


class LeNormalize(object):
    """Normalize to -1..1 in Google Inception style
    """

    def __call__(self, tensor):
        for t in tensor:
            t.sub_(0.5).mul_(2.0)
        return tensor

def showtmp(img):
    imgtmp = (img-img.min())/(img.max() - img.min())
    plt.imshow(imgtmp)
    plt.show()
    plt.close()

def save_defense(img, dfs_path, name, dfs_int_path=''):
    np.save(dfs_path+'/'+name, img)
    if dfs_int_path != '':
        imgtmp = (img-img.min())/(img.max() - img.min())
        fig = np.around(imgtmp)
        fig = fig.astype(np.uint8).squeeze()
        pic = Image.fromarray(fig)
        # pic.resize((512,512), resample=PIL.Image.BICUBIC)
        pic.save(dfs_int_path+'/'+name)

def main():
    raise Exception("this denoiser method doesn't make sense since it cannot defend CW L2, look at the paper, they only tried FGSM defense.")
    args = parser.parse_args()
    # import ipdb;ipdb.set_trace()
    if not os.path.exists(args.noise_dir):
        print("Error: Invalid input folder %s" % args.noise_dir)
        exit(-1)
    if not args.output_dir:
        print("Error: Please specify an output file")
        exit(-1)
        
    tf = transforms.Compose([
           # transforms.Scale([299,299]),
            transforms.ToTensor()
    ])

    mean_torch = autograd.Variable(torch.from_numpy(np.array([0.485, 0.456, 0.406]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    std_torch = autograd.Variable(torch.from_numpy(np.array([0.229, 0.224, 0.225]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    mean_tf = autograd.Variable(torch.from_numpy(np.array([0.5, 0.5, 0.5]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    std_tf = autograd.Variable(torch.from_numpy(np.array([0.5, 0.5, 0.5]).reshape([1,3,1,1]).astype('float32')).cuda(), volatile=True)
    

    dataset = Dataset(args.noise_dir,transform=tf)
    loader = data.DataLoader(dataset, batch_size=args.batch_size, shuffle=False,drop_last=False)
    noise_type = os.path.splitext(dataset.filenames()[0])[-1]

    if args.orig_dir != '':
        dataset2 = Dataset(args.orig_dir,transform=tf)
        loader2 = data.DataLoader(dataset2, batch_size=args.batch_size, shuffle=False,drop_last=False)    
        orig_type = os.path.splitext(dataset2.filenames()[0])[-1]

    config, incepv3model = get_model3()
    net3 = incepv3model.net
    checkpoint = torch.load('denoise_incepv3_012.ckpt')
    if isinstance(checkpoint, dict) and 'state_dict' in checkpoint:
        incepv3model.load_state_dict(checkpoint['state_dict'])
    else:
        incepv3model.load_state_dict(checkpoint)

    if not args.no_gpu:
        incepv3model = incepv3model.cuda()
    incepv3model.eval()

    if args.task == 'label':
        if args.ensemble:
            config, resmodel = get_model1()
            config, inresmodel = get_model2()
            config, rexmodel = get_model4()
            net1 = resmodel.net    
            net2 = inresmodel.net
            net4 = rexmodel.net

            checkpoint = torch.load('denoise_res_015.ckpt')
            if isinstance(checkpoint, dict) and 'state_dict' in checkpoint:
                resmodel.load_state_dict(checkpoint['state_dict'])
            else:
                resmodel.load_state_dict(checkpoint)

            checkpoint = torch.load('denoise_inres_014.ckpt')
            if isinstance(checkpoint, dict) and 'state_dict' in checkpoint:
                inresmodel.load_state_dict(checkpoint['state_dict'])
            else:
                inresmodel.load_state_dict(checkpoint)
            checkpoint = torch.load('denoise_rex_001.ckpt')
            if isinstance(checkpoint, dict) and 'state_dict' in checkpoint:
                rexmodel.load_state_dict(checkpoint['state_dict'])
            else:
                rexmodel.load_state_dict(checkpoint)

            if not args.no_gpu:
                inresmodel = inresmodel.cuda()
                resmodel = resmodel.cuda()
                rexmodel = rexmodel.cuda()
                inresmodel.eval()
                resmodel.eval()
                rexmodel.eval()

    # outputs = []
    dfs_path = os.path.join(args.output_dir, "dfs")
    dfs_int_path = os.path.join(args.output_dir, "dfs_int")
    os.system("mkdir -p {}".format(dfs_path))

    denoiser = net3.denoise
    total_cnter = 0
    defend_cnt = 0
    attack_cnt = 0
    denoise_crrct = 0
    orig_def_red = 0
    labels_noise = []
    labels_denoise = []
    labels_orig = []
    labels_orig_dfs = []
    for batch_idx, (input, fname) in enumerate(loader):
        # loaded from adv, already [-1,1], shape need to be modified 
        # input = torch.Tensor(input)
        if not args.no_gpu:
            input = input.cuda()
        input_var = autograd.Variable(input, volatile=True) # loaded from adv, already [-1,1]
        if noise_type == '.npy':
            input_var = input_var.permute([0,3,1,2])
        denoised = denoiser(input_var)
        if args.task == 'denoise':
            denoised = denoised.permute([0,2,3,1])
            for j in range(denoised.shape[0]):
                # showtmp(denoised[j].cpu().data.numpy())
                save_defense(denoised[j].cpu().data.numpy(), dfs_path, fname[j].replace("adversarial","defense"))
        elif args.task == 'label':
            input_01 = input_var.clone()# back to 0 1
            input_01 = input_01 * std_tf + mean_tf
            input_torch = (input_01 - mean_torch)/std_torch
            labels3_noise = net3(input_var,False)[-1]
            labels3_denoise = net3(input_var,True)[-1]
            if args.ensemble:
                labels1_noise = net1(input_torch,False)[-1]
                labels2_noise = net2(input_var,False)[-1]
                labels4_noise = net4(input_torch,False)[-1]
                label_noise = (labels1_noise+labels2_noise+labels3_noise+labels4_noise).max(1)[1] + 1
                labels1_denoise = net1(input_torch,True)[-1]
                labels2_denoise = net2(input_var,True)[-1]
                labels4_denoise = net4(input_torch,True)[-1]
                label_denoise = (labels1_denoise+labels2_denoise+labels3_denoise+labels4_denoise).max(1)[1] + 1
            else:
                label_noise = labels3_noise.max(1)[1] + 1
                label_denoise = labels3_denoise.max(1)[1] + 1
            labels_noise += [label_noise]
            labels_denoise += [label_denoise]
            # labels_noise = net3(input_var,False)[-1]
            # labels_dns = net3(input_var,True)[-1]
            # labels_denoise = net3(denoised,False)[-1]
            # print("pred on denoised == denoise\&pred?", str((labels_dns == labels_denoise).all()))
            # labels_noise = labels_noise.max(1)[1] + 1
            # labels_denoise = labels_denoise.max(1)[1] + 1
    if args.task == 'label':
        for batch_idx, (input2, fname) in enumerate(loader2):
        # loaded from adv, already [-1,1], shape need to be modified 
        # input = torch.Tensor(input)
            if not args.no_gpu:
                input2 = input2.cuda()
            input_var2 = autograd.Variable(input2, volatile=True)
            if orig_type == '.png':
                input_tf2 = (input_var2 - mean_tf)/std_tf
                input_torch2 = (input_var2 - mean_torch)/std_torch
            elif orig_type == '.npy':
                input_tf2 = input_var2.clone().permute([0,3,1,2])
                # back to 0 1
                input2_01 = input_tf2 * std_tf + mean_tf
                input_torch2 = (input2_01 - mean_torch)/std_torch
            labels3_orig = net3(input_tf2,False)[-1]
            if args.ensemble:
                labels1_orig = net1(input_torch2,False)[-1]
                labels2_orig = net2(input_tf2,False)[-1]
                labels4_orig = net4(input_torch2,False)[-1]
                label_orig = (labels1_orig+labels2_orig+labels3_orig+labels4_orig).max(1)[1] + 1
            else:
                label_orig = labels3_orig.max(1)[1] + 1
            labels_orig += [label_orig]
            # labels1_orig_dfs = net1(input_torch2,True)[-1]
            # labels2_orig_dfs = net2(input_tf2,True)[-1]
            
            # labels4_orig_dfs = net4(input_torch2,True)[-1]
            # labels_orig_dfs = (labels1_orig_dfs+labels2_orig_dfs+labels3_orig_dfs+labels4_orig_dfs).max(1)[1] + 1

        labels_noise = torch.cat(labels_noise)
        labels_denoise = torch.cat(labels_denoise)
        labels_orig = torch.cat(labels_orig)
        # redundant = labels_orig == labels_orig_dfs
        attacked = labels_noise != labels_orig
        correct = labels_denoise == labels_orig
        # orig_def_red = len(torch.nonzero(redundant))
        attack_cnt = len(torch.nonzero(attacked))
        denoise_crrct = len(torch.nonzero(correct))
        defend_cnt = len(torch.nonzero(attacked&correct))
        print("{} attacked(clf), out of {}, among which {} defended".format(attack_cnt, len(labels_orig), defend_cnt))
        print("{} defended result consistent with orig".format(denoise_crrct))

    #     labels1 = net1(input_torch,True)[-1]
    #     labels2 = net2(input_tf,True)[-1]
    #     labels3 = net3(input_tf,True)[-1]
    #     labels4 = net4(input_torch,True)[-1]

    #     labels = (labels1+labels2+labels3+labels4).max(1)[1] + 1  # argmax + offset to match Google's Tensorflow + Inception 1001 class ids
    #     outputs.append(labels.data.cpu().numpy())
    # outputs = np.concatenate(outputs, axis=0)

    # with open(args.output_file, 'w') as out_file:
    #     filenames = dataset.filenames()
    #     for filename, label in zip(filenames, outputs):
    #         filename = os.path.basename(filename)
    #         out_file.write('{0},{1}\n'.format(filename, label))

if __name__ == '__main__':
    main()
