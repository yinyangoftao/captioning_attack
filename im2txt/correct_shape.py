import os
import numpy as np

path = "/media/zhe/hhd/targeted800_2000"
for filename in os.listdir(path):
	if filename.endswith(".npy"):
		im1 = np.load(path+'/'+filename).reshape(299,299,3)
		np.save(path+'/'+filename, im1)