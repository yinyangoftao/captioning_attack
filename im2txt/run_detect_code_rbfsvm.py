# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Generate captions for images using default beam search parameters."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import os
import re
import csv
# from tqdm import tqdm
import random
import numpy as np
import tensorflow as tf
import torch
from torch.autograd import Variable as Variable
import torch.nn as nn
import torch.nn.functional as F
import configuration
import inference_wrapper
from inference_utils import caption_generator
from inference_utils import vocabulary
import matplotlib.pyplot as plt
import torchvision.models as models
from sklearn import svm
from feature_extractor import InceptionFeatureExtractor

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string("checkpoint_path", "pretrained/model2.ckpt-2000000",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")

tf.flags.DEFINE_string("vocab_file", "pretrained/word_counts.txt", 
                      "Text file containing the vocabulary.")

tf.flags.DEFINE_string("orig_dir", "",
                       "Input original image directory of original images.")

tf.flags.DEFINE_string("adv_dir", "",
                       "Input attacked image directory of original images.")

tf.flags.DEFINE_string("dfs_dir", "",
                       "Input defended attacked image directory of original images.")

tf.flags.DEFINE_string("coding", "lstm",
                       "What kind of defense to use.")

tf.flags.DEFINE_string("result_directory", "../result_dir/",
                        "the directory to save results")

tf.flags.DEFINE_bool(
    'defend_orig', True, 'Whether defend original image.')

tf.flags.DEFINE_bool(
    'tf2_01', False, 'Convert from torch value domain to [0,1] domain.')

tf.flags.DEFINE_integer("beam_size", 3, "beam search size")

tf.flags.DEFINE_integer(
    'image_width', 299, 'Width of each input images.')

tf.flags.DEFINE_integer(
    'image_height', 299, 'Height of each input images.')

tf.flags.DEFINE_integer(
    'batch_size', 1, 'How many images process at one time.')

tf.flags.DEFINE_integer(
    'image_resize', 331, 'Resize of image size.')

tf.logging.set_verbosity(tf.logging.INFO)

def padding_layer_iyswim(inputs, shape, name=None):
    # resize the image and pad it, the returned image would be resized to a square again
    # inputs is the size of the resized image
    # shape is [vertical start padding len, horizontal start padding len, output short len]

    h_start = shape[0]
    w_start = shape[1]
    output_short = shape[2]
    input_shape = tf.shape(inputs)
    input_short = tf.reduce_min(input_shape[1:3])
    input_long = tf.reduce_max(input_shape[1:3])
    output_long = tf.to_int32(tf.ceil(
        1. * tf.to_float(output_short) * tf.to_float(input_long) / tf.to_float(input_short)))
    output_height = tf.to_int32(input_shape[1] >= input_shape[2]) * output_long +\
        tf.to_int32(input_shape[1] < input_shape[2]) * output_short
    output_width = tf.to_int32(input_shape[1] >= input_shape[2]) * output_short +\
        tf.to_int32(input_shape[1] < input_shape[2]) * output_long
    return tf.pad(inputs, tf.to_int32(tf.stack([[0, 0], [h_start, output_height - h_start - input_shape[1]], [w_start, output_width - w_start - input_shape[2]], [0, 0]])), name=name)

def print_inception_v3_code(net):
  for param in net.modules():
    print(type(param))
    # if type(param) == nn.ReLU:
    #   print("relu layers",param)
    # if type(param) == nn.Linear:
    #   print("linear layers",param)

def main(_):

  import ipdb; ipdb.set_trace()
  record_path = FLAGS.result_directory
  beam_size = FLAGS.beam_size
  mean_torch_np = np.array([0.485, 0.456, 0.406]).reshape(1,1,3)
  std_torch_np = np.array([0.229, 0.224, 0.225]).reshape(1,1,3)
  mean_tf_np = np.array([0.5, 0.5, 0.5]).reshape(1,1,3)
  std_tf_np = np.array([0.5, 0.5, 0.5]).reshape(1,1,3)

  if FLAGS.coding == 'lstm':
    g = tf.Graph()
    with g.as_default():
      model = inference_wrapper.InferenceWrapper()
      restore_fn = model.build_graph_from_config(configuration.ModelConfig(),
                                                 FLAGS.checkpoint_path)
      # preprocessing compute graph
      image_placeholder = tf.placeholder(dtype=tf.string, shape=[])
      preprocessor = model.model.process_image(image_placeholder)

    g.finalize()

  adv_filenames = [file for file in os.listdir(FLAGS.adv_dir+'advs/')]
  # real valued images
  orig_npy_filenames = [file for file in os.listdir(FLAGS.orig_dir+'origs_npy/')]
  adv_npy_filenames = [file for file in os.listdir(FLAGS.adv_dir+'advs_npy/')]
  # orig_filenames.sort()
  adv_filenames.sort()
  orig_npy_filenames.sort()
  adv_npy_filenames.sort()
  tf.logging.info("Running caption generation on %d files matching %s",
                  len(orig_npy_filenames), FLAGS.orig_dir)

  orig_codes_trn = []
  adv_codes_trn = []
  orig_codes_tst = []
  adv_codes_tst = []
  if FLAGS.coding == 'lstm':
    sess = tf.Session(graph=g)
    restore_fn(sess)
  elif FLAGS.coding == 'inc_v3':
    net = models.inception_v3(pretrained=True).eval().cuda()
    # inception_v3_code(net)
    code_extractor = InceptionFeatureExtractor(net)
  file_N = len(adv_npy_filenames)
  for i in range(file_N):
    #read in a batch and process together
    print(i)
    np_orig_img = np.squeeze(np.load(FLAGS.orig_dir+'origs_npy/'+orig_npy_filenames[i]))
    np_adv_img = np.squeeze(np.load(FLAGS.adv_dir+'advs_npy/'+adv_npy_filenames[i]))
    if FLAGS.tf2_01:
      # np_adv_img = (np.clip(np_adv_img * std_tf_np + mean_tf_np, 0,1) - mean_torch_np)/std_torch_np
      np_adv_img = ((np_adv_img * std_tf_np + mean_tf_np) - mean_torch_np)/std_torch_np
      np_orig_img = ((np_orig_img * std_tf_np + mean_tf_np) - mean_torch_np)/std_torch_np
    if FLAGS.coding == 'lstm':
      benign_code = model.feed_image(sess, np_orig_img)
      adv_code = model.feed_image(sess, np_adv_img)
    elif FLAGS.coding == 'inc_v3':
      np_orig_img = Variable(torch.Tensor(np_orig_img).cuda(), volatile = True).permute([2,0,1]).unsqueeze(0)
      np_adv_img = Variable(torch.Tensor(np_adv_img).cuda(), volatile = True).permute([2,0,1]).unsqueeze(0)
      benign_code = code_extractor(np_orig_img).view(1,-1).cpu().data.numpy()
      adv_code = code_extractor(np_adv_img).view(1,-1).cpu().data.numpy()
    # if i < 0.2*file_N:
    #   orig_codes_tst += [benign_code]
    #   adv_codes_tst += [adv_code]
    # else:
    orig_codes_trn += [benign_code]
    adv_codes_trn += [adv_code]
  orig_codes_trn = np.vstack(orig_codes_trn)
  # adv_codes_tst = np.vstack(adv_codes_tst)
  # orig_codes_tst = np.vstack(orig_codes_tst)
  adv_codes_trn = np.vstack(adv_codes_trn)
  codes_trn = np.vstack([orig_codes_trn, adv_codes_trn])
  label_trn = np.hstack([np.ones(len(orig_codes_trn)), np.zeros(len(adv_codes_trn))])
  # codes_tst = np.vstack([orig_codes_tst,adv_codes_tst])
  # label_tst = np.hstack([np.ones(len(orig_codes_tst)), np.zeros(len(adv_codes_tst))])
  bins = np.arange(-1,1,0.4)
  codes_qnt_trn = np.digitize(codes_trn, bins)
  # codes_qnt_tst = np.digitize(codes_tst, bins)
  adv_detector = svm.SVC()
  adv_detector.fit(codes_qnt_trn, label_trn)
  res = adv_detector.score(codes_qnt_trn, label_trn)
  print(res)
  if FLAGS.coding == 'lstm':
    sess.close()


if __name__ == "__main__":
  tf.app.run()
